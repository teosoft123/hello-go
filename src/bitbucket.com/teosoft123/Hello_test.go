package main

import (
	"testing"
)

func TestHello(t *testing.T) {
	e := "Hello world."
	if a := Hello(); a != e {
		t.Errorf("Expected <%s>, actual <%s>", e, a)
	}
}
